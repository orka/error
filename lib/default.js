/**
 * Common error types definitions
 */
define(['p!-defineclass', 'p!-assertful', 'p!-logger'], function($classify, $assertful, $logger) {
    'use strict';

    var type = {
        ARGUMENTS: 'ARGUMENTS',
        TYPE_ERROR: 'TYPE_ERROR',
        NETWORK: 'NETWORK',
        GENERIC: 'GENERIC',
        UNDEFINED: 'UNDEFINED'
    };

    var ErrorClass = $classify('Error', {
        constructor: function(__type, __message) {
            if ($assertful.undef(__type) || $assertful.undef(__message)) {
                throw new ReferenceError('Error type and message must be specified');
            }

            this.time = Date.now();
            this.type = __type;
            this.message = __message;
            this.args = Array.prototype.slice.apply(arguments, [2]);
            //create stack
            this.stack = new Error().stack.split('\n');
            //access this instance (not a Class) .class and retrieve ancestry array
            this.stack.splice(0, this.class.lineage.length + 3);
            this.stack = this.stack.join('\n\t');
            this.__string = this.classname + '(' + this.type + ') "' + this.message + '"\n\ttime: ' + this.time + '\n' + this.stack;
            if (ErrorClass.__DEBUG__ === true) {
                this.log();
            }
        },

        deconstructor: function() {
            this.type = null;
            this.message = null;
            this.args = null;
            this.stack = null;
        },

        /**
         * Outputs stack trace of error that only includes relavent info
         * @returns {Object} this instance
         */
        trace: function() {
            $logger.error.apply($logger, [this.__string]);
            return this;
        },

        /**
         * uses $logger.error to output att the data assocciated with this error
         * @returns {Object} this instance
         */
        log: function() {
            $logger.error.apply($logger, [this.__string].concat(this.args));
            return this;
        },

        /**
         * Throws error
         */
        throw: function() {
            throw new Error(this.__string);
        },

        TYPE: type
    });

    /**
     * MErges TYPE of Super to a subclass
     * @param   {Subclass} __class chils class
     * @param   {Superclass} __super GENESES Class
     */
    ErrorClass.onSubclassCreated = function(__class, __super) {
        var _i, _key, _keys = Object.keys(__super.api.TYPE);
        //normalize TYPE of subclass
        __class.api.TYPE = __class.api.TYPE || __super.api.TYPE;

        for (_i = 0; _i < _keys.length; _i += 1) {
            _key = _keys[_i];
            __class.api.TYPE[_key] = __super.api.TYPE[_key];
        }
    };

    ErrorClass.TYPE = type;
    return ErrorClass;
});
