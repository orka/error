/*
 *  Tests error throw and type APIS
 *
 */
define(['lib/default'], function($error) {
    'use strict';

    describe('Error Type', function() {
        it('Should exist', function() {
            expect($error).toBeDefined();
        });

        it('.TYPE Should exist', function() {
            expect($error.TYPE).toBeDefined();
        });

        it('.TYPE.ARGUMENTS Should exist', function() {
            expect($error.TYPE.ARGUMENTS).toBeDefined();
        });

        it('.TYPE.NETWORK Should exist', function() {
            expect($error.TYPE.NETWORK).toBeDefined();
        });

        it('.TYPE.GENERIC Should exist', function() {
            expect($error.TYPE.GENERIC).toBeDefined();
        });

        it('Undefined type of message should throw', function() {
            expect($error.bind(null)).toThrow();
            expect($error.bind(null, '')).toThrow();
            expect($error.bind(null, '', '')).toThrow();
        });

        it('Should create error instance', function() {
            expect($error($error.TYPE.GENERIC, 'message')).toBeDefined();
        });

        it('Should throw on untyped TYPE', function() {
            expect($error('$error.TYPE.GENERIC__sd', 'message').type).toEqual($error.TYPE.UNDEFINED);
        });

        it('Should thow on instance.throw()', function() {
            expect($error($error.TYPE.GENERIC, 'message').throw).toThrow();
        });

        it('Should .log() error + args', function() {
            console.log('Should .log() error + args');
            expect($error($error.TYPE.GENERIC, 'message', $error.TYPE).log()).toBeDefined();
        });

        it('Should .trace() error', function() {
            console.log('Should .trace() error');
            expect($error($error.TYPE.GENERIC, 'message', $error.TYPE).trace()).toBeDefined();
        });

        it('Should .log() error subclass', function() {
            console.log('Should .log() error subclass');
            var _sub = $error.subclass('SubErrorType', {
                constructor: function() {
                    _sub.super.constructor.apply(this, arguments);
                },
                deconstructor: function() {

                }
            });
            expect(_sub($error.TYPE.GENERIC, 'message', $error.TYPE).log()).toBeDefined();
        });

        it('Should .log() error subclass of a subclass', function() {
            console.log('Should .log() error subclass');
            var _sub = $error.subclass('SubErrorType', {
                constructor: function() {
                    _sub.super.constructor.apply(this, arguments);
                },
                deconstructor: function() {

                }
            });

            var _subSub = $error.subclass('SubErrorTypeSubclass', {
                constructor: function() {
                    _sub.super.constructor.apply(this, arguments);
                },
                deconstructor: function() {

                }
            });
            expect(_subSub($error.TYPE.GENERIC, 'message', $error.TYPE).log()).toBeDefined();
        });
    });
});
